
all:
	@echo "Nothing to do... try \"clean\""

clean:
	rm -f *~
	rm -f *.nav
	rm -f *.pdf
	rm -f *.vrb
	rm -f *.snm
	rm -f *.tex
